# Food-Camp Ansible playbooks explained

Ansible playbooks to configure remote hosts and deploy app

## install-prerequisites

The first step, which updates the host system and installs required packages and docker.

## configure-nginx

This playbook is used to perform simple nginx configuration with transferring `.conf` file to hosts and configure certbot for ssl sertificates.

## deploy-compose-app

Set of instructions to deploy main site via docker compose on remote hosts

## Author

- Krazhevskiy Aleksey
  - [github](https://github.com/alekseykrazhev)
  - [gitlab](https://gitlab.com/alekseykrazhev)
  - [dockerhub](https://hub.docker.com/u/alekseykrazhev)
